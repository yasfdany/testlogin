
package ga.studiocloud.testlogin.Models.ListMahasiswa;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Mahasiswa {

    @SerializedName("mahasiswa_id")
    @Expose
    private Integer mahasiswaId;
    @SerializedName("mahasiswa_name")
    @Expose
    private String mahasiswaName;
    @SerializedName("mahasiswa_address")
    @Expose
    private String mahasiswaAddress;
    @SerializedName("mahasiswa_phone")
    @Expose
    private String mahasiswaPhone;

    public Integer getMahasiswaId() {
        return mahasiswaId;
    }

    public void setMahasiswaId(Integer mahasiswaId) {
        this.mahasiswaId = mahasiswaId;
    }

    public String getMahasiswaName() {
        return mahasiswaName;
    }

    public void setMahasiswaName(String mahasiswaName) {
        this.mahasiswaName = mahasiswaName;
    }

    public String getMahasiswaAddress() {
        return mahasiswaAddress;
    }

    public void setMahasiswaAddress(String mahasiswaAddress) {
        this.mahasiswaAddress = mahasiswaAddress;
    }

    public String getMahasiswaPhone() {
        return mahasiswaPhone;
    }

    public void setMahasiswaPhone(String mahasiswaPhone) {
        this.mahasiswaPhone = mahasiswaPhone;
    }

}
