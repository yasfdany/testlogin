package ga.studiocloud.testlogin.Adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;
import ga.studiocloud.testlogin.Activities.ListMahasiswaActivity;
import ga.studiocloud.testlogin.Models.ListMahasiswa.Mahasiswa;
import ga.studiocloud.testlogin.R;

import static ga.studiocloud.testlogin.Tools.Constan.Alert;
import static ga.studiocloud.testlogin.Tools.Constan.Form;
import static ga.studiocloud.testlogin.Tools.Constan.klien;

public class MahasiswaAdapter extends RecyclerView.Adapter<MahasiswaAdapter.holder> {
    private final ProgressDialog mprogress;
    Context konteks;
    List<Mahasiswa> data;

    public MahasiswaAdapter(Context konteks, List<Mahasiswa> data) {
        this.konteks = konteks;
        this.data = data;
        mprogress = new ProgressDialog(konteks);
        mprogress.setCancelable(false);
        mprogress.setMessage("Memuat...");
    }

    @NonNull
    @Override
    public holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(konteks).inflate(R.layout.item_mahasiswa, parent, false);

        return new holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull holder holder, int position) {
        Mahasiswa mahasiswa = data.get(position);
        holder.mNamaItem.setText(mahasiswa.getMahasiswaName());
        holder.mAlamatItem.setText(mahasiswa.getMahasiswaAddress());
        holder.mIdItem.setText(mahasiswa.getMahasiswaId()+"");
        holder.mNomerItem.setText(mahasiswa.getMahasiswaPhone());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void hapusMahasiswa(final Mahasiswa mahasiswa){
        mprogress.show();
        RequestParams requestParams = new RequestParams();
        requestParams.put("id",mahasiswa.getMahasiswaId());

        klien.post("http://gabut.intivestudio.com/api/v1/mahasiswa/delete", requestParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                AlertDialog alert = Alert(konteks, "Sambungan gagal", throwable.getLocalizedMessage() + "");
                alert.show();
                mprogress.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                mprogress.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(responseString);
                    boolean status = jsonObject.getBoolean("status");
                    if (status){
                        Toast.makeText(konteks,jsonObject.getString("message")+"",Toast.LENGTH_SHORT).show();
                        data.remove(mahasiswa);
                        notifyDataSetChanged();
                    } else {
                        AlertDialog alert = Alert(konteks,"Sambungan gagal",jsonObject.getString("message")+"");
                        alert.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void editMahasiswa(final String nama, final String alamat, final String nomer, final Mahasiswa mahasiswa){
        mprogress.show();
        RequestParams requestParams = new RequestParams();
        requestParams.put("id",mahasiswa.getMahasiswaId());
        requestParams.put("name",nama);
        requestParams.put("address",alamat);
        requestParams.put("phone",nomer);

        klien.post("http://gabut.intivestudio.com/api/v1/mahasiswa/update", requestParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                AlertDialog alert = Alert(konteks, "Sambungan gagal", throwable.getLocalizedMessage() + "");
                alert.show();
                mprogress.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                mprogress.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(responseString);
                    boolean status = jsonObject.getBoolean("status");
                    if (status){
                        Toast.makeText(konteks,jsonObject.getString("message")+"",Toast.LENGTH_SHORT).show();
                        mahasiswa.setMahasiswaName(nama);
                        mahasiswa.setMahasiswaAddress(alamat);
                        mahasiswa.setMahasiswaPhone(nomer);
                        notifyDataSetChanged();
                    } else {
                        AlertDialog alert = Alert(konteks,"Sambungan gagal",jsonObject.getString("message")+"");
                        alert.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public class holder extends RecyclerView.ViewHolder {
        TextView mIdItem;
        TextView mNamaItem;
        TextView mAlamatItem;
        TextView mNomerItem;
        public holder(View view) {
            super(view);
            mIdItem = (TextView) view.findViewById(R.id.id_item);
            mNamaItem = (TextView) view.findViewById(R.id.nama_item);
            mAlamatItem = (TextView) view.findViewById(R.id.alamat_item);
            mNomerItem = (TextView) view.findViewById(R.id.nomer_item);

            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(konteks);
                    alert.setMessage("Hapus?");
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            hapusMahasiswa(data.get(getAdapterPosition()));
                        }
                    });
                    alert.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });

                    alert.show();
                    return true;
                }
            });

            view.setOnClickListener(new View.OnClickListener() {
                public AlertDialog alert;
                public List<Object> alerForm;

                @Override
                public void onClick(View view) {
                    View.OnClickListener listener = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            AlertDialog alert = (AlertDialog) alerForm.get(0);
                            EditText nama = (EditText) alerForm.get(1);
                            EditText alamat = (EditText) alerForm.get(2);
                            EditText nomer = (EditText) alerForm.get(3);

                            editMahasiswa(nama.getText().toString(),alamat.getText().toString(),nomer.getText().toString(),data.get(getAdapterPosition()));
                            alert.dismiss();
                        }
                    };
                    alerForm = Form(konteks,listener);
                    EditText nama = (EditText) alerForm.get(1);
                    EditText alamat = (EditText) alerForm.get(2);
                    EditText nomer = (EditText) alerForm.get(3);

                    nama.setText(data.get(getAdapterPosition()).getMahasiswaName());
                    alamat.setText(data.get(getAdapterPosition()).getMahasiswaAddress());
                    nomer.setText(data.get(getAdapterPosition()).getMahasiswaPhone());

                    alert = (AlertDialog) alerForm.get(0);
                    alert.show();
                }
            });
        }
    }
}
