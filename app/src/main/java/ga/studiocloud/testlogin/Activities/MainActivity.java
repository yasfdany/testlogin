package ga.studiocloud.testlogin.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import ga.studiocloud.testlogin.Models.ListMahasiswa.ListMahasiswa;
import ga.studiocloud.testlogin.R;
import ga.studiocloud.testlogin.Tools.Constan;

import static ga.studiocloud.testlogin.Tools.Constan.Alert;
import static ga.studiocloud.testlogin.Tools.Constan.klien;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    /**
     * E - Mail
     */
    private EditText mEmail;
    /**
     * Password
     */
    private EditText mPassword;
    /**
     * LOGIN
     */
    private Button mLoginButton;
    private ProgressDialog progress;
    private SharedPreferences dbPref;
    private SharedPreferences.Editor dbPrefEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

        dbPref = getSharedPreferences("db",MODE_PRIVATE);
        dbPrefEdit = dbPref.edit();

        String token = dbPref.getString("token", null);
        if (token!=null){
            Intent intent = new Intent(MainActivity.this, ListMahasiswaActivity.class);
            startActivity(intent);
            finish();
        }

        progress = new ProgressDialog(this);
        progress.setMessage("Tunggu...");
        progress.setCancelable(true);
    }

    public void Login(String email, String password){
        progress.show();
        RequestParams requestParams = new RequestParams();
        requestParams.put("email",email);
        requestParams.put("password",password);

        klien.setTimeout(30000);
        klien.post("http://gabut.intivestudio.com/api/v1/auth/login/siswa", requestParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                AlertDialog alert = Alert(MainActivity.this,"Sambungan gagal",throwable.getLocalizedMessage()+"");
                alert.show();
                progress.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                progress.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(responseString);
                    boolean status = jsonObject.getBoolean("status");
                    if (status){
                        dbPrefEdit.putString("token",jsonObject.getString("data"));
                        dbPrefEdit.commit();
                        Toast.makeText(MainActivity.this,"Login Berhasil",Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(MainActivity.this, ListMahasiswaActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        AlertDialog alert = Alert(MainActivity.this,"Sambungan gagal",jsonObject.getString("message")+"");
                        alert.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void initView() {
        mEmail = (EditText) findViewById(R.id.email);
        mPassword = (EditText) findViewById(R.id.password);
        mLoginButton = (Button) findViewById(R.id.login_button);
        mLoginButton.setOnClickListener(this);
    }

    public boolean checkError(){
        if (mEmail.getText().toString().isEmpty()){
            mEmail.setError("Tidak boleh kosong");
            return true;
        }

        if (mPassword.getText().toString().isEmpty()){
            mPassword.setError("Tidak boleh kosong");
            return true;
        }

        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.login_button:
                if (!checkError()){
                    Login(mEmail.getText().toString(),mPassword.getText().toString());
                }
                break;
        }
    }
}
