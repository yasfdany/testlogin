package ga.studiocloud.testlogin.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import ga.studiocloud.testlogin.Adapters.MahasiswaAdapter;
import ga.studiocloud.testlogin.Models.ListMahasiswa.ListMahasiswa;
import ga.studiocloud.testlogin.Models.ListMahasiswa.Mahasiswa;
import ga.studiocloud.testlogin.R;

import static ga.studiocloud.testlogin.Tools.Constan.Alert;
import static ga.studiocloud.testlogin.Tools.Constan.Form;
import static ga.studiocloud.testlogin.Tools.Constan.gson;
import static ga.studiocloud.testlogin.Tools.Constan.klien;

public class ListMahasiswaActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView mListMahasiswa;
    List<Mahasiswa> data;
    private MahasiswaAdapter mMahasiswaAdapter;
    private ProgressDialog mprogress;
    private FloatingActionButton mAddMahasiswa;
    private List<Object> alerForm;
    private AlertDialog alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_mahasiswa_activity);
        initView();

        data = new ArrayList<>();

        mMahasiswaAdapter = new MahasiswaAdapter(this, data);
        mListMahasiswa.setLayoutManager(new LinearLayoutManager(this));
        mListMahasiswa.setAdapter(mMahasiswaAdapter);

        mprogress = new ProgressDialog(this);
        mprogress.setCancelable(false);
        mprogress.setMessage("Memuat...");

        getListMahasiswa();
    }

    public void getListMahasiswa() {
        mprogress.show();
        klien.setTimeout(30000);
        klien.get("http://gabut.intivestudio.com/api/v1/mahasiswa/list", new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                AlertDialog alert = Alert(ListMahasiswaActivity.this, "Sambungan gagal", throwable.getLocalizedMessage() + "");
                alert.show();
                mprogress.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                mprogress.dismiss();
                ListMahasiswa listMahasiswa = gson.fromJson(responseString, ListMahasiswa.class);
                data.addAll(listMahasiswa.getData());

                mMahasiswaAdapter.notifyDataSetChanged();
            }
        });
    }

    public void addMahasiswa(String nama, String alamat, String nomer){
        mprogress.show();
        RequestParams requestParams = new RequestParams();
        requestParams.put("name",nama);
        requestParams.put("address",alamat);
        requestParams.put("phone",nomer);

        klien.post("http://gabut.intivestudio.com/api/v1/mahasiswa/create", requestParams, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                AlertDialog alert = Alert(ListMahasiswaActivity.this, "Sambungan gagal", throwable.getLocalizedMessage() + "");
                alert.show();
                mprogress.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                mprogress.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(responseString);
                    boolean status = jsonObject.getBoolean("status");
                    if (status){
                        data.clear();
                        Toast.makeText(ListMahasiswaActivity.this,jsonObject.getString("message")+"",Toast.LENGTH_SHORT).show();
                        getListMahasiswa();
                    } else {
                        AlertDialog alert = Alert(ListMahasiswaActivity.this,"Sambungan gagal",jsonObject.getString("message")+"");
                        alert.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void initView() {
        mListMahasiswa = (RecyclerView) findViewById(R.id.list_mahasiswa);
        mAddMahasiswa = (FloatingActionButton) findViewById(R.id.add_mahasiswa);
        mAddMahasiswa.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.add_mahasiswa:
                View.OnClickListener listener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog alert = (AlertDialog) alerForm.get(0);
                        EditText nama = (EditText) alerForm.get(1);
                        EditText alamat = (EditText) alerForm.get(2);
                        EditText nomer = (EditText) alerForm.get(3);

                        addMahasiswa(nama.getText().toString(),alamat.getText().toString(),nomer.getText().toString());
                        alert.dismiss();
                    }
                };
                alerForm = Form(ListMahasiswaActivity.this,listener);
                alert = (AlertDialog) alerForm.get(0);
                alert.show();
                break;
        }
    }
}
