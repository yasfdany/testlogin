package ga.studiocloud.testlogin.Tools;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;

import java.util.ArrayList;
import java.util.List;

import ga.studiocloud.testlogin.R;

public class Constan {
    public static AsyncHttpClient klien = new AsyncHttpClient();
    public static Gson gson = new Gson();

    public static AlertDialog Alert(Context konteks, String judul, String pesan) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(konteks);
        alertDialog.setTitle(judul);
        alertDialog.setMessage(pesan);

        alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertCreate = alertDialog.create();

        return alertCreate;
    }

    public static List<Object> Form(Context konteks, View.OnClickListener onclick) {
        List<Object> objReturned = new ArrayList<>();

        View view = LayoutInflater.from(konteks).inflate(R.layout.dialog_form, null);
        EditText mNamaForm = (EditText) view.findViewById(R.id.nama_form);
        EditText mAlamatForm = (EditText) view.findViewById(R.id.alamat_form);
        EditText mNomerForm = (EditText) view.findViewById(R.id.nomer_form);
        Button mButtonSimpan = (Button) view.findViewById(R.id.button_simpan);

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(konteks);
        alertDialog.setView(view);
        AlertDialog alertCreate = alertDialog.create();

        objReturned.add(alertCreate);
        objReturned.add(mNamaForm);
        objReturned.add(mAlamatForm);
        objReturned.add(mNomerForm);

        mButtonSimpan.setOnClickListener(onclick);

        return objReturned;
    }
}
